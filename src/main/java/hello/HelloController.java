
package hello;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

//@RestController
@Controller
public class HelloController {
	
	@ModelAttribute
	public void setResponseHeader(HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("X-Powered-By", "JDK16");
        response.setHeader("X-Backend-Server", "apache-tomcat-9.0.52");
	}	
	
    @GetMapping({"/"})
    public String home() {
        return "login";
    }

    @GetMapping({"/hello"})
    public String hello(Model model, @RequestParam(value="userName", required=false, defaultValue="World") String userName) {
    	model.addAttribute("userName", userName);
    	return "hello";
    }
    
    @GetMapping({"/mysql-error"})
    public String mysqlError() {
        return "mysql-error";
    }
    
    @PostMapping("/login")
    public String login(Model model, @RequestParam(value="userName", required=false) String userName, @RequestParam(value="password", required=false) String password) {
      model.addAttribute("userName", userName);
      model.addAttribute("password", password);
      return "hello";
    }    

 
}
