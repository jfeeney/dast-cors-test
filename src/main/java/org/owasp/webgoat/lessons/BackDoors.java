package org.owasp.webgoat.lessons;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.ecs.Element;
import org.apache.ecs.ElementContainer;
import org.apache.ecs.StringElement;
import org.apache.ecs.html.A;
import org.apache.ecs.html.BR;
import org.apache.ecs.html.Div;
import org.apache.ecs.html.IMG;
import org.apache.ecs.html.Input;
import org.apache.ecs.html.PRE;
import org.apache.ecs.html.TD;
import org.apache.ecs.html.TH;
import org.apache.ecs.html.TR;
import org.apache.ecs.html.Table;
import org.owasp.webgoat.session.DatabaseUtilities;


/***************************************************************************************************
 * 
 * 
 * This file is part of WebGoat, an Open Web Application Security Project utility. For details,
 * please see http://www.owasp.org/
 * 
 * Copyright (c) 2002 - 20014 Bruce Mayhew
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 * 
 * Getting Source ==============
 * 
 * Source for this application is maintained at https://github.com/WebGoat/WebGoat, a repository for free software
 * projects.
 * 
 * For details, please see http://webgoat.github.io
 * 
 * @author Sherif Koussa <a href="http://www.softwaresecured.com">Software Secured</a>
 */
public class BackDoors
{

    private final static String SELECT_ST = "select userid, password, ssn, salary, email from employee where userid=";


    protected Element concept1(String userInput) throws Exception
    {
        ElementContainer ec = new ElementContainer();

        ec.addElement(userInput);

        try
        {
            if (!userInput.equals(""))
            {
                userInput = SELECT_ST + userInput;
                String[] arrSQL = userInput.split(";");
                Connection conn = DatabaseUtilities.getConnection();
                Statement statement = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                                                            ResultSet.CONCUR_READ_ONLY);

                ResultSet rs = statement.executeQuery(arrSQL[0]);
                addDBEntriesToEC(ec, rs);

            }
        } catch (Exception ex)
        {
            ec.addElement(new PRE(ex.getMessage()));
        }
        return ec;
    }

    protected Element concept2(String userName, String userInput) throws Exception
    {
        ElementContainer ec = new ElementContainer();
        ec.addElement(userName);


        if (!userInput.equals(""))
        {
            userInput = SELECT_ST + userInput;
            String[] arrSQL = userInput.split(";");
            Connection conn = DatabaseUtilities.getConnection();
            Statement statement = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

            ResultSet rs = statement.executeQuery(arrSQL[0]);
            addDBEntriesToEC(ec, rs);

        }
        return ec;
    }


    public String getTitle()
    {
        return ("Database Backdoors ");
    }
    
    public void addDBEntriesToEC(ElementContainer ec, ResultSet rs) {
    	
    	// to be completed
 
    }
}
