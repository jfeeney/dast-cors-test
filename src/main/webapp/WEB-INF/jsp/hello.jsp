<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Hello ${userName}!</title>
    <link href="/css/main.css" rel="stylesheet">
    <script src="http://code.jquery.com/mobile/1.1.2/jquery.mobile-1.1.2.min.js"></script>
</head>
<body>
    <h2 class="hello-title">Hello ${userName}!</h2>
    <p/>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec faucibus elit libero, at commodo diam suscipit nec. Interdum et malesuada fames ac ante ipsum primis in faucibus. Proin mattis laoreet diam, sit amet maximus justo tempor nec. Etiam tincidunt aliquam risus eget rhoncus. Mauris non ultricies leo. Mauris vitae purus lorem. Vivamus quam ex, pulvinar id sapien non, placerat ornare dolor. In hac habitasse platea dictumst. Ut libero ligula, viverra ut lectus vestibulum, dictum vestibulum nunc. Aliquam felis est, mattis non porttitor vel, eleifend a quam. Curabitur auctor arcu eu augue congue, a tempus dui tempus.</p>
	<p/>
	<p>Aliquam vehicula finibus tincidunt. Aliquam dolor metus, maximus eget imperdiet eu, placerat sed tortor. Cras eget interdum dolor. Aliquam vitae leo ultricies nunc volutpat interdum. Sed fringilla dolor viverra, hendrerit turpis nec, bibendum lacus. Ut vestibulum purus consectetur aliquam tempus. Quisque vestibulum, sapien ac consequat semper, dolor enim fermentum arcu, vel malesuada libero mauris ac magna. Proin tincidunt accumsan diam eget facilisis. Nulla pharetra eu purus eget dictum. Pellentesque mattis pharetra ipsum a interdum. Duis vel varius risus. Integer lobortis, sapien et vehicula blandit, felis purus cursus ipsum, ut eleifend ex risus non arcu. Nunc tincidunt turpis nisi, eget mollis nulla molestie ut. Maecenas quis tincidunt metus. Etiam et porta lacus. Donec varius lorem tincidunt tempus sollicitudin.</p>
	<p/>
	<p>Mauris eget tortor consectetur, efficitur lacus vel, accumsan tortor. Vestibulum venenatis tellus id mauris vehicula tempus. Vivamus sagittis rutrum eros et pulvinar. In ornare maximus pulvinar. Pellentesque ultrices dolor eget purus condimentum luctus. Integer et lacinia est. Aenean luctus metus erat, cursus hendrerit justo auctor at. Praesent et porttitor justo. Phasellus pulvinar dignissim augue in aliquam. Aliquam accumsan lorem nulla, ultrices consequat tellus feugiat vitae.</p>

    
    <script src="/js/main.js"></script>

</body>
</html>
